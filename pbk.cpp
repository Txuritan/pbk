#include <iostream>
#include <map>

#include "json.hpp"
#include "sqlite3.h"
#include "afterlife.hpp"

void api(Afterlife::Request *req, Afterlife::Response *res) {
    // std::map<std::string, std::string>::iterator iter;
    // for (iter = req->headers.begin(); iter != req->headers.end(); ++iter) {
    //     std::cout << iter->first << " = " << iter->second << std::endl;
    // }

    res->body << "Hello World!";
}

int main() {
    try {
        Afterlife::Server server;
        server.get("/", &api);
        server.start("127.0.0.1", 8800);
    } catch (Afterlife::Exception e) {
        std::cerr << Afterlife::Log::error << e.what() << std::endl;
    }

    return EXIT_SUCCESS;
}
