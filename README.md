# Portable Bookmarks

## Building

Dependencies:

- CMake
- Linux
  - Ninja (or some other build system)
- Windows
  - Visual Studio Build Tools

### Linux

```shell
$ mkdir build && cd build
$ cmake .. -G "Ninja" -DCMAKE_BUILD_TYPE=Release
$ ninja -C .
```

### Windows

```shell
$ mkdir build && cd build
$ cmake .. -G "Visual Studio 15 2017" -DCMAKE_BUILD_TYPE=Release
$ msbuild pbk.vcxproj
```