// TODO
//   Thread pooling
//   Response writer
//   Get Windows working (custom socket class)

#pragma once

#if defined(_WIN32) || defined(_WIN64)
#define OS_WINDOWS
#elif defined(__APPLE__)
#define OS_MACOS
#elif defined(__unix__) || defined(__unix)
#define OS_LINUX
#else
#error unsupported platform
#endif

#include <algorithm>
#include <atomic>
#include <cstdio>
#include <ctime>
#include <cctype>
#include <exception>
#include <fstream>
#include <functional>
#include <future>
#include <iostream>
#include <map>
#include <memory>
#include <mutex>
#include <queue>
#include <regex>
#include <sstream>
#include <thread>
#include <utility>
#include <vector>

#if defined(OS_MACOS) || defined(OS_LINUX)
    #include <arpa/inet.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <sys/stat.h>
    #include <limits.h>
    #include <stdlib.h>
    #include <string.h>
    #include <unistd.h>
#elif defined(OS_WINDOWS)
    #include <stdint.h>
    #include <stdio.h>
    #include <io.h>
    #include <winsock2.h>
    #include <windows.h>

    #pragma comment(lib, "Ws2_32.lib")
#endif

#define SERVER_NAME "Afterlife"
#define SERVER_VERSION "1.0.0"

#define BUFSIZE 8096

namespace Afterlife {
    // From https://github.com/ikalnytskyi/termcolor
    namespace Color {
        namespace _internal {
            static int colorize_index = std::ios_base::xalloc();

            inline FILE *get_standard_stream(const std::ostream &stream) {
                if (&stream == &std::cout)
                    return stdout;
                else if ((&stream == &std::cerr) || (&stream == &std::clog))
                    return stderr;

                return 0;
            }

            inline bool is_atty(const std::ostream &stream) {
                FILE *std_stream = get_standard_stream(stream);

                if (!std_stream)
                    return false;

                #if defined(OS_MACOS) || defined(OS_LINUX)
                    return ::isatty(fileno(std_stream));
                #elif defined(OS_WINDOWS)
                    return ::_isatty(_fileno(std_stream));
                #endif
            }

            inline bool is_colorized(std::ostream &stream) {
                return is_atty(stream) || static_cast<bool>(stream.iword(colorize_index));
            }

            #if defined(OS_WINDOWS)
            inline void win_change_attributes(std::ostream &stream, int foreground, int background = -1) {
                static WORD defaultAttributes = 0;

                if (!_internal::is_atty(stream))
                    return;

                HANDLE hTerminal = INVALID_HANDLE_VALUE;
                if (&stream == &std::cout)
                    hTerminal = GetStdHandle(STD_OUTPUT_HANDLE);
                else if (&stream == &std::cerr)
                    hTerminal = GetStdHandle(STD_ERROR_HANDLE);

                if (!defaultAttributes) {
                    CONSOLE_SCREEN_BUFFER_INFO info;
                    if (!GetConsoleScreenBufferInfo(hTerminal, &info))
                        return;
                    defaultAttributes = info.wAttributes;
                }

                if (foreground == -1 && background == -1) {
                    SetConsoleTextAttribute(hTerminal, defaultAttributes);
                    return;
                }

                CONSOLE_SCREEN_BUFFER_INFO info;
                if (!GetConsoleScreenBufferInfo(hTerminal, &info))
                    return;

                if (foreground != -1) {
                    info.wAttributes &= ~(info.wAttributes & 0x0F);
                    info.wAttributes |= static_cast<WORD>(foreground);
                }

                if (background != -1) {
                    info.wAttributes &= ~(info.wAttributes & 0xF0);
                    info.wAttributes |= static_cast<WORD>(background);
                }

                SetConsoleTextAttribute(hTerminal, info.wAttributes);
            }
            #endif
        }

        inline std::ostream &colorize(std::ostream &stream) {
            stream.iword(_internal::colorize_index) = 1L;
            return stream;
        }

        inline std::ostream &nocolorize(std::ostream &stream) {
            stream.iword(_internal::colorize_index) = 0L;
            return stream;
        }

        inline std::ostream &reset(std::ostream &stream) {
            if (_internal::is_colorized(stream)) {
                #if defined(OS_MACOS) || defined(OS_LINUX)
                    stream << "\033[00m";
                #elif defined(OS_WINDOWS)
                    _internal::win_change_attributes(stream, -1, -1);
                #endif
            }
            return stream;
        }

        inline std::ostream &red(std::ostream &stream) {
            if (_internal::is_colorized(stream)) {
                #if defined(OS_MACOS) || defined(OS_LINUX)
                    stream << "\033[31m";
                #elif defined(OS_WINDOWS)
                    _internal::win_change_attributes(stream, FOREGROUND_RED);
                #endif
            }
            return stream;
        }

        inline std::ostream &green(std::ostream &stream) {
            if (_internal::is_colorized(stream)) {
                #if defined(OS_MACOS) || defined(OS_LINUX)
                    stream << "\033[32m";
                #elif defined(OS_WINDOWS)
                    _internal::win_change_attributes(stream, FOREGROUND_GREEN);
                #endif
            }
            return stream;
        }

        inline std::ostream &yellow(std::ostream &stream) {
            if (_internal::is_colorized(stream)) {
                #if defined(OS_MACOS) || defined(OS_LINUX)
                    stream << "\033[33m";
                #elif defined(OS_WINDOWS)
                    _internal::win_change_attributes(stream, FOREGROUND_GREEN | FOREGROUND_RED);
                #endif
            }
            return stream;
        }

        inline std::ostream &white(std::ostream &stream) {
            if (_internal::is_colorized(stream)) {
                #if defined(OS_MACOS) || defined(OS_LINUX)
                    stream << "\033[37m";
                #elif defined(OS_WINDOWS)
                    _internal::win_change_attributes(stream, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
                #endif
            }
            return stream;
        }
    }

    namespace Log {
        inline std::ostream &error(std::ostream &stream) {
            std::string date_time;

            char buffer[100];
            time_t now = time(0);
            struct tm tstruct = *gmtime(&now);
            strftime(buffer, sizeof(buffer), "%a, %d %b %Y %H:%M:%S %Z", &tstruct);
            date_time = buffer;

            stream << "[" << date_time << "]  " << Color::red << "ERROR " << Color::reset;
            return stream;
        }

        inline std::ostream &info(std::ostream &stream) {
            std::string date_time;

            char buffer[100];
            time_t now = time(0);
            struct tm tstruct = *gmtime(&now);
            strftime(buffer, sizeof(buffer), "%a, %d %b %Y %H:%M:%S %Z", &tstruct);
            date_time = buffer;

            stream << "[" << date_time << "]  " << Color::green << "INFO  " << Color::reset;
            return stream;
        }
    }

    class Pool {};

    class Socket {
        public:
        private:
    };

    class Request {
        public:
            std::string method;
            std::string path;
            std::string params;

            std::map<std::string, std::string> headers;
            std::map<std::string, std::string> query;
            std::map<std::string, std::string> cookies;

            std::string body;

            Request() {}

            static void parse(char *body, Request *req) {
                int index = 0;

                std::istringstream body_stream(body);
                std::string line;

                bool is_body = false;

                while (std::getline(body_stream, line)) {
                    if (line == "") {
                        is_body = true;
                    }

                    if (is_body) {
                        req->body = req->body + line + "\n";
                    } else {
                        if (index == 0) {
                            Request::parse_request(line, req);
                        } else {
                            Request::parse_header(line, req);
                        }
                    }

                    index++;
                }
            };

        private:
            static void parse_request(std::string line, Request *req) {
                std::vector<std::string> split_line;

                Request::split(line, ": ", 2, &split_line);

                req->method = split_line[0];
                req->path = split_line[1];

                size_t pos = req->path.find('?');

                if (pos != std::string::npos) {
                    Request::parse_params(pos, req->path.substr(pos + 1), req);

                    req->path = req->path.substr(0, pos);
                }
            };

            static void parse_params(size_t pos, std::string line, Request *req) {
                std::vector<std::string> params;

                Request::split(line, "&", -1, &params);

                for(std::vector<std::string>::size_type q = 0; q < params.size(); q++) {
                    std::vector<std::string> query;

                    Request::split(params[q], "=", -1, &query);

                    if (query.size() == 2) {
                        req->query[query[0]] = query[1];
                    }
                }
                
            };

            static void parse_header(std::string line, Request *req) {
                std::vector<std::string> split_line;

                Request::split(line, ": ", 2, &split_line);

                if (split_line.size() == 2) {
                    if (split_line[0] == "Cookie") {
                        Request::parse_cookies(split_line[1], req);
                    } else {
                        req->headers[split_line[0]] = split_line[1];
                    }
                }
            };

            static void parse_cookies(std::string line, Request *req) {
                std::vector<std::string> cookies;

                Request::split(line, "; ", -1, &cookies);

                for (std::vector<std::string>::size_type c = 0; c < cookies.size(); c++) {
                    std::vector<std::string> cookie;

                    Request::split(cookies[c], "=", 2, &cookie);

                    req->cookies[cookie[0]] = cookie[1];
                }
            };

            static void split(std::string str, std::string separator, int max, std::vector<std::string> *results) {
                int i = 0;
                size_t found = str.find_first_of(separator);

                while (found != std::string::npos) {
                    if (found > 0) {
                        results->push_back(str.substr(0, found));
                    }

                    str = str.substr(found + 1);
                    found = str.find_first_of(separator);

                    if (max > -1 && ++i == max) break;
                }

                if (str.length() > 0) {
                    results->push_back(str);
                }
            };
    };

    class Response {
        public:
            int code;

            std::string phrase;
            std::string type;
            std::string date;

            std::map<std::string, std::string> headers;

            std::stringstream body;

            Response() {
                code = 200;

                phrase = "OK";
                type = "text/html";

                char buffer[100];
                time_t now = time(0);
                struct tm tstruct = *gmtime(&now);
                strftime(buffer, sizeof(buffer), "%a, %d %b %Y %H:%M:%S %Z", &tstruct);
                date = buffer;

                body << "";
            }

            void set_header(std::string key, std::string value) {
                headers.insert(std::pair<std::string, std::string>(key, value));
            }

            void send(std::string str) {
                body << str;
            };

            void send(const char* str) {
                body << str;
            };
    };

    struct Route {
        std::string path;
        std::string method;

        std::function<void(Request*, Response*)> callback;

        std::string params;
    };

    class Exception : public std::exception {
        public:
            Exception() : pMessage("") {}
            Exception(const char* pStr) : pMessage(pStr) {}

            const char* what() const throw () {
                return pMessage;
            }

        private:
            const char* pMessage;
    };

    std::vector<Route> ROUTES;

    class Server {
        public:
            void all(std::string path, std::function<void(Request*, Response*)> callback) {
                this->add_route(path, "ALL", callback);
            };

            void del(std::string path, std::function<void(Request*, Response*)> callback) {
                this->add_route(path, "DELETE", callback);
            };

            void get(std::string path, std::function<void(Request*, Response*)> callback) {
                this->add_route(path, "GET", callback);
            };

            void patch(std::string path, std::function<void(Request*, Response*)> callback) {
                this->add_route(path, "PATCH", callback);
            };

            void post(std::string path, std::function<void(Request*, Response*)> callback) {
                this->add_route(path, "POST", callback);
            };

            void put(std::string path, std::function<void(Request*, Response*)> callback) {
                this->add_route(path, "PUT", callback);
            };

            bool start(const char* host, int port) {
                std::cout << Log::info << "Starting server on " << host << ":" << port << std::endl;

                this->main_loop(host, &port);

                return true;
            };

        private:
            std::string trim(std::string s) {
                s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int c) { return !std::isspace(c); }));
                s.erase(std::find_if(s.begin(), s.end(), [](int c) { return !std::isspace(c); }), s.end());

                return s;
            };

            void split(std::string str, std::string separator, int max, std::vector<std::string> *results) {
                int i = 0;
                size_t found = str.find_first_of(separator);

                while (found != std::string::npos) {
                    if (found > 0) {
                        results->push_back(str.substr(0, found));
                    }

                    str = str.substr(found + 1);
                    found = str.find_first_of(separator);

                    if (max > -1 && ++i == max) break;
                }

                if (str.length() > 0) {
                    results->push_back(str);
                }
            };

            void add_route(std::string path, std::string method, std::function<void(Request*, Response*)> callback) {
                Route r = {
                    path,
                    method,
                    callback,
                    ""
                };

                ROUTES.push_back(r);
            };

            bool match_route(Request *req, Response *res) {
                for (std::vector<Route>::size_type i = 0; i < ROUTES.size(); i++) {
                    std::regex pattern(ROUTES[i].path);

                    if ((regex_match(req->path, pattern) || ROUTES[i].path == req->path) && (ROUTES[i].method == req->method || ROUTES[i].method == "ALL")) {
                        req->params = ROUTES[i].params;

                        ROUTES[i].callback(req, res);

                        return true;
                    }
                }

                return false;
            };

            void *main_loop(const char *host, int *port) {
                #if defined(OS_MACOS) || defined(OS_LINUX)
                    int new_conn;

                    int socket_conn = socket(AF_INET, SOCK_STREAM, 0);

                    if (socket_conn < 0) {
                        throw Exception("ERROR opening socket");
                    }
                #elif defined(OS_WINDOWS)
                    WSADATA wsaData;
                    WSAStartup(0x0101, &wsaData);

                    SOCKET new_conn;

                    SOCKET socket_conn = socket(AF_INET, SOCK_STREAM, 0);
                #endif

                struct sockaddr_in serv_addr, cli_addr;
                serv_addr.sin_family = AF_INET;
                serv_addr.sin_addr.s_addr = inet_addr(host);
                serv_addr.sin_port = htons(*port);

                if (::bind(socket_conn, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) != 0) {
                    throw Exception("ERROR on binding");
                }

                listen(socket_conn, 5);

                #if defined(OS_MACOS) || defined(OS_LINUX)
                    socklen_t clilen;
                    clilen = sizeof(cli_addr);
                #elif defined(OS_WINDOWS)
                    int clilen;
                    clilen = sizeof(cli_addr);
                #endif

                while (true) {
                    #if defined(OS_MACOS) || defined(OS_LINUX)
                        new_conn = accept(socket_conn, (struct sockaddr *) &cli_addr, &clilen);

                        if (new_conn < 0) {
                            throw Exception("ERROR on accept");
                        }
                    #elif defined(OS_WINDOWS)
                        new_conn = accept(socket_conn, (struct sockaddr *) &cli_addr, &clilen);
                    #endif

                    Request req;
                    Response res;

                    static char headers[BUFSIZE + 1];

                    #if defined(OS_MACOS) || defined(OS_LINUX)
                        long ret = read(new_conn, headers, BUFSIZE);
                    #elif defined(OS_WINDOWS)
                        long ret = _read(new_conn, headers, BUFSIZE);
                    #endif

                    if (ret > 0 && ret < BUFSIZE) {
                        headers[ret] = 0;
                    } else {
                        headers[0] = 0;
                    }

                    Request::parse(headers, &req);

                    if (!this->match_route(&req, &res)) {
                        res.code = 404;
                        res.phrase = "Not Found";
                        res.type = "text/plain";
                        res.send("Not found");
                    }

                    std::cout << Log::info << Color::yellow << res.code << Color::reset << "  " << req.path << std::endl;

                    char header_buffer[BUFSIZE];
                    std::string body = res.body.str();
                    size_t body_len = body.size();

                    #if defined(OS_MACOS) || defined(OS_LINUX)
                        sprintf(header_buffer, "HTTP/1.0 %d %s\r\n", res.code, res.phrase.c_str());

                        sprintf(&header_buffer[strlen(header_buffer)], "Server: %s %s\r\n", SERVER_NAME, SERVER_VERSION);
                        sprintf(&header_buffer[strlen(header_buffer)], "Date: %s\r\n", res.date.c_str());
                        sprintf(&header_buffer[strlen(header_buffer)], "Content-Type: %s\r\n", res.type.c_str());
                        sprintf(&header_buffer[strlen(header_buffer)], "Content-Length: %zd\r\n", body_len);

                        std::map<std::string, std::string>::iterator iter;
                        for (iter = res.headers.begin(); iter != res.headers.end(); ++iter) {
                            sprintf(&header_buffer[strlen(header_buffer)], "%s: %s\r\n", iter->first.c_str(), iter->second.c_str());
                        }

                        strcat(header_buffer, "\r\n");

                        write(new_conn, header_buffer, strlen(header_buffer));
                        write(new_conn, body.c_str(), body_len);
                    #elif defined(OS_WINDOWS)
                        sprintf_s(header_buffer, sizeof(header_buffer), "HTTP/1.0 %d %s\r\n", res.code, res.phrase.c_str());

                        sprintf_s(&header_buffer[strlen(header_buffer)], sizeof(header_buffer), "Server: %s %s\r\n", SERVER_NAME, SERVER_VERSION);
                        sprintf_s(&header_buffer[strlen(header_buffer)], sizeof(header_buffer), "Date: %s\r\n", res.date.c_str());
                        sprintf_s(&header_buffer[strlen(header_buffer)], sizeof(header_buffer), "Content-Type: %s\r\n", res.type.c_str());
                        sprintf_s(&header_buffer[strlen(header_buffer)], sizeof(header_buffer), "Content-Length: %zd\r\n", body_len);

                        std::map<std::string, std::string>::iterator iter;
                        for (iter = res.headers.begin(); iter != res.headers.end(); ++iter) {
                            sprintf_s(&header_buffer[strlen(header_buffer)], sizeof(header_buffer), "%s: %s\r\n", iter->first.c_str(), iter->second.c_str());
                        }

                        strcat_s(header_buffer, sizeof(header_buffer), "\r\n");

                        _write(new_conn, header_buffer, strlen(header_buffer));
                        _write(new_conn, body.c_str(), body_len);
                    #endif
                }
            };
    };
}
